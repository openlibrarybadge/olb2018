# Kooperative Entwicklung der Kriterien für den Open Library Badge 2020

Die Entstehung der Kriterien [dokumentieren wir im Wiki](https://gitlab.com/openlibrarybadge/olb2020/wikis/home).
Später werden hier dann die verabschiedeten Kriterien abgelegt.

Mehr Infos auf der Webseite <https://badge.openbiblio.eu>